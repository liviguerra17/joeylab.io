import Path from "path"
import {File, Folder, SiteGenerator} from "./kickstand/index.mjs"
import MicrodataParser from "./MicrodataParser.mjs"
import jsdom from "jsdom"
const __dirname = Path.resolve()

const {JSDOM, VirtualConsole} = jsdom
const makeDom = html => {
    let c = new VirtualConsole()
    c.sendTo(console)
    return new JSDOM(html, {
        url: "https://example.org/",
        referrer: "https://example.org/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 5000000,
        pretendToBeVisual: true,
        runScripts: "dangerously"
    })
}

const root = Path.resolve()
const layoutsFolder = Path.join(root, "/templates/layouts")
const pagesFolder = Path.join(root, "/templates/pages")
const BLOG_INDEX_TEMPLATE_PATH = `${pagesFolder}/blog/index.html`
export default {
    createFolders(){
        Promise.all([Folder.create("templates"),
            Folder.create("templates/layouts"),
            Folder.create("templates/pages"),
            Folder.create("public")]
        ).then(v=>{
            console.log("Folders created")
        }).catch(e => {
            console.error(e)
        })
        return SiteGenerator
    },
    async cleanupBeforeStarting(){
        try{
            await Folder.delete(`${root}/public/blog/`)
        }catch(e){}
    },
    async createBlogIndexTemplateWithPosts(posts) {
        let html = ""
        try{
            let data= await File.readFile(BLOG_INDEX_TEMPLATE_PATH, {encoding: "utf8"})
            let obj = SiteGenerator.renderPageObject(BLOG_INDEX_TEMPLATE_PATH, data)
            posts.sort((a, b)=>{
                let aPublished = new Date(a.published)
                let bPublished = new Date(b.published)
                if(aPublished.getTime() == bPublished.getTime()) return 0
                if(aPublished.getTime() > bPublished.getTime()) return 1
                return -1
            })
            html = obj.template(posts.reverse())
        } catch(e){
            console.log(e)
        }
        return html
    },
    async copyFoldersOver(folders, destination){
        folders.forEach(async key=>{
            let folder = Path.resolve(__dirname, key)
            let files = await Folder.read(folder)
            let root = folder.split(Path.sep).pop()
            for await (let file of files) {
                await Folder.copy(file, file.replace(folder, `${destination}${Path.sep}${root}`))
            }
        })    
    },
    async run(delegate = {
        finished(files){}
    }, publicFolder = "./public"){
        try{
            await this.cleanupBeforeStarting()
        }catch(e){
            console.error("cleanup", e)
        }
        try{
            await Folder.create("public/blog")
        }catch(e){

        }
        try{
            await this.copyFoldersOver(["webapp", "resources/css", "resources/.well-known",
                "resources/favicon_package_v0", "resources/favicons", "resources/imgs"], publicFolder)
        }catch(e){
            throw e
        }

        let posts = []
        let blogPostPattern = /\/blog\/\d+/
        const cssFiles = []
        SiteGenerator.run(pagesFolder, layoutsFolder)
        .then(async files => {
            const domFiles = []
            for(let i = 0; i < files.length; i++){
                let t = files[i]
                let newFilePath = t.file.replace(pagesFolder, "")
                let relativeLink = newFilePath.replace(/^\//, "")
                let context = {
                    meta: {
                        uri: newFilePath,
                        relativeLink
                    }
                }
                let filePath = Path.join(root, "/public/", newFilePath)
                
                if(/\.css/.test(filePath)){
                    //console.log(filePath.split("/public/").pop())
                    cssFiles.push({
                        filePath: filePath,
                        data: t.data
                    })
                }
                try{
                    const interpolated = t.template(context)
                    if(filePath.indexOf(".html") > -1){
                        domFiles.push({
                            data: interpolated,
                            filePath: filePath
                        })
                    }

                    try{await Folder.create(Path.dirname(filePath))}catch(e){}
                    let b = await File.writeFile(filePath, interpolated)
                    const parser = MicrodataParser()
                    const dom = makeDom(interpolated)
                    parser.execute(dom.window.document.body, scope => {
                        if(isHTMLFormatedBlogPost(scope)) {
                            context.meta.published = new Date(scope.properties.findByKey("published").value)
                            context.meta.should_publish = context.meta.published <= new Date() ? "yes" : "no"
                            context.meta.title = scope.properties.findByKey("headline").value
                            context.meta.headline = context.meta.title
                            context.meta.summary = scope.properties.findByKey("abstract").value
                        }
                    })
                    if(context.meta.published) context.meta.displayDate = context.meta.published.toLocaleDateString(undefined, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit' })
                    if(blogPostPattern.test(t.file) && context.meta.should_publish === "yes"){
                        try{
                            let stat = await File.stat(Path.join(root, "/templates/pages/", newFilePath).replace(/.html$/, ".md"))
                            context.meta.birthtime = stat.birthtime
                        }catch(e){}
                        posts.push(context.meta)
                    }
                } catch(e){
                    console.error("in running", t.file, e)
                }
            }
            const doms = domFiles.map(file => this.swapLinkWithStyle(file, cssFiles))
            let output = await this.createBlogIndexTemplateWithPosts(posts)
            let blog = doms.find(f=>f.filePath.indexOf("blog/index.html") > -1)
            blog.data = output
            blog.data = this.swapLinkWithStyle(blog, cssFiles).data
            await File.writeFile(Path.join(root, "/public/blog/", "index.html"), output)
            delegate.finished(doms)
        }).catch(e=>{
            console.log(e)
        })
    },
    swapLinkWithStyle(file, cssFiles){
        const dom = makeDom(file.data)
        const links = Array.prototype.slice.call(dom.window.document.querySelectorAll("link[rel='stylesheet']"))
        links.forEach(link => {
            const href = link.href.replace("https://example.org/", "")
            const sheets = cssFiles.filter(styleSheet => styleSheet.filePath.indexOf(href) > -1)
            sheets.forEach(sheet => {
                const style = dom.window.document.createElement("style")
                style.textContent = sheet.data
                link.parentNode.insertBefore(style, link)
            })
            if(sheets.length > 0) link.parentNode.removeChild(link)
        })
        //console.log(file.filePath, `start: ${dom.window.document.documentElement.innerHTML} :end`)
        return {
            filePath: file.filePath,
            data: `<!doctype html>${dom.window.document.documentElement.outerHTML}`
        }
    }
}
function isHTMLFormatedBlogPost(scope) {
    return scope.types.indexOf("http://schema.org/BlogPosting") > -1
}

