import assert from 'assert';
describe('testing message passing', ()=>{
    it('should pass a message', async ()=>{
        const receiver = {
            async exec(m){
                this.didReceiveMessage = true;
                return m;
            },
            didReceiveMessage: false
        };
        
        const sender = {
            async send(r, m){
                return await r.exec(m);
            }
        };

        await sender.send(receiver, 'some message');
        assert.strictEqual(receiver.didReceiveMessage, true);
    });
});