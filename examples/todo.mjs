import tdding from "./Tdd.mjs"
import jsdom from "jsdom"
import Fs from "fs"
import Path from "path"

const File = Fs.promises
const __dirname = Path.resolve()
const {JSDOM, VirtualConsole} = jsdom
const makeDom = html => {
    let c = new VirtualConsole()
    c.sendTo(console)
    return new JSDOM(html, {
        url: "https://example.org/",
        referrer: "https://example.org/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 5000000,
        pretendToBeVisual: true
    })
}

tdding.push("Entering text and submitting adds an item", async t => {
    const data = await File.readFile(Path.join(__dirname, "/public/todos.html"), {encoding: "utf8"})
    const dom = makeDom(data)
    t.ok(dom, "Should make a dom")
})

export default tdding