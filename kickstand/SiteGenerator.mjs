import Dir from "./Dir.mjs"
import handlebars from "handlebars"
import path from "path"
import Template from "./Template.mjs"
import debug from "debug"
const log = debug("SiteGenerator")
const {File, Folder} = Dir
const SiteGenerator = {
    registerPartial(key, data){
        handlebars.registerPartial(key, data)
    },
    renderPageObject(pathName, text){
        return {
            template: Template.get(path.extname(pathName), text),
            data: text,
            file: Template.transformToHtml(pathName)
        }
    },
    async run(pagesFolder, layoutsFolder){
        let layouts = await Folder.read(layoutsFolder)
        for(let i = 0; i < layouts.length; i++){
            let layoutFileName = layouts[i]
            let data = await File.readFile(layoutFileName, {encoding: "utf8"})
            SiteGenerator.registerPartial(layoutFileName.split(path.sep).pop(), data)
        }
        let pages = await Folder.read(pagesFolder)
        let files = []
        for(let i = 0; i < pages.length; i++){
            try{
                let p = pages[i]
                let file = await File.readFile(p, {encoding: "utf8"})
                let obj = SiteGenerator.renderPageObject(p, file)
                files.push( obj )
            }catch(e){console.error(e)}
        }
        return files
    }
}
export default SiteGenerator