let connections = 0
const limiterFilter = {
    beginning(req, res, next){
        const start = Date.now()
        connections++
        res.once("finish", () => {
            connections--
            const diffInSeconds = (Date.now() - start)/1000
            //console.log(`${req.url} #${connections} ${diffInSeconds} sec.`)
        })
        next()
    }
}

export default options => {
    return limiterFilter
}