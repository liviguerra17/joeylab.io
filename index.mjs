import express from "express"
import http from "http"
import builder from "./builder.mjs"
import Path from "path"
import WebSocketServer from "./WebSocketServer.mjs"
import LimiterFilter from "./LimiterFilter.mjs"
import ContentNegotiatedViewEngine from "./ContentNegotiatedViewEngine.mjs"

const __dirname = Path.resolve()
const app = express()
const port = process.env.PORT || 3000
const PUBLIC_FOLDER = Path.resolve(__dirname, "./public")
const limiter = LimiterFilter()
builder.createFolders()
express.static.mime.define({"text/javascript": ["js", "mjs"]})
app.set("view", ContentNegotiatedViewEngine)
app.use(limiter.beginning)
app.use(express.static(PUBLIC_FOLDER, {
    setHeaders: (res, path) => {
        if(path.indexOf(".well-known") === -1) return
        res.setHeader("Content-Type", "text/plain")
    }
}))
app.use("/webapp", express.static(Path.join(__dirname, "./webapp")))
app.use("/socketIo", express.static(Path.join(__dirname, "node_modules/socket.io-client/dist")))

app.get("/health", (req, res)=>{
    res.send("Ok")
})
app.get("/testing", (req, res)=>{
    app.render("testing", {Message: "testing"})
})

const server = http.createServer(app).listen(port, () => {
    console.log(`Listening on http://localhost:${port}`)
})
let diff = {
    files: [],
    room: null
}
const io = WebSocketServer(server, {
    left(room, socket){},
    joined(room, socket){
        diff.room = room
        diff.user = socket.request.user
    },
    messageFromUser(message){},
    updated(message, room, socket){
        console.log("updated")
    }
})

;(async ()=>{
    await builder.run({
        finished(files){
            let interval = setInterval(()=>{
                if(!diff.room) return
                clearInterval(interval)
                const file = files.find(file => {
                    const u = Path.parse(file.filePath.replace(PUBLIC_FOLDER, ""))
                    let filePath = ""
                    if(u.dir == "/") filePath = u.base
                    else filePath = `${u.dir}/${u.base}`
                    const pattern = new RegExp(filePath)
                    return pattern.test(diff.room)
                })
                if(!file) return
                io.to(diff.room).emit("update", file.data)
            }, 300)
        }
    }, PUBLIC_FOLDER)
})()

export default app