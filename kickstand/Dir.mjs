import Fs from "fs"
import Path from "path"
const File = Fs.promises
const Folder = {
    async read(folder){
        let files = await File.readdir(folder)
        let justFiles = []
        let upper = files.length
        for(let i = 0; i < upper; i++){
            let f = Path.join(folder, files[i])
            let stat = await File.stat(f)
            if(stat.isDirectory()){
                justFiles = justFiles.concat(await Folder.read(f))
            } else {
                justFiles.push(f)
            }
        }
        return justFiles
    },
    async copy(from, to){
        const folders = to.split(Path.sep)
        folders.pop()
        let folderPath = "/"
        for(let i = 0; i < folders.length; i++){
            folderPath = Path.resolve(folderPath, folders[i])
            try{
                await File.stat(folderPath)
            }catch(e){
                try{await this.create(folderPath)}catch(e){}
            }
        }
        Fs.createReadStream(from)
            .pipe(Fs.createWriteStream(to))
    },
    async delete(folderPathName){
        let files = await this.read(folderPathName)
        let upper = files.length
        for(let i = 0; i < upper; i++){
            await File.unlink(files[i])
        }
        let folders = await File.readdir(folderPathName)
        let foldersMax = folders.length
        for(let i = 0; i < foldersMax; i++){
            try{
                await File.rmdir(`${folderPathName}/${folders[i]}`)
            }catch(e){console.log(e)}
        }
        await File.rmdir(folderPathName)
    },
    async create(folder){
        await File.mkdir(folder)
    }
}

export default {
    File: File,
    Folder: Folder
}