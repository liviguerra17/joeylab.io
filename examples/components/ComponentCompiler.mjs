import tdding from "../Tdd.mjs"
import jsdom from "jsdom"
import fs from "fs"
import path from "path"

const File = fs.promises
const moduleURL = new URL(import.meta.url);
const __dirname = path.dirname(moduleURL.pathname);
const {JSDOM, VirtualConsole} = jsdom
const makeDom = html => {
    let c = new VirtualConsole()
    c.sendTo(console)
    return new JSDOM(html, {
        url: "https://example.org/",
        referrer: "https://example.org/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 5000000,
        pretendToBeVisual: true,
        runScripts: "dangerously"
    })
}
tdding.push("Build a component into a single file", async t => {
    const data = await File.readFile(path.join(__dirname, "search.html"), {encoding: "utf8"})
    const dom = makeDom(data)
    const searchField = dom.window.document.querySelector("input[type='search']")
    const list = dom.window.document.querySelector("ul")
    searchField.value = "some term in here"
    t.ok(dom, "Should make a dom")
    const actual = list.querySelectorAll("li").length
    const scripts = dom.window.document.querySelector("script")
    t.ok(actual > 0, `Should have an item in it ${actual}`)
})

export default tdding