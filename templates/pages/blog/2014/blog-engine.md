---
layout: post.html
title: I'm a programmer, I'm coding my own blog engine
summary: |
    This has been a long time coming. I've preached about blogging for years and yet, have never done it myself. Well, I have but back in 1997 and I didn't keep it up and I don't have proof. And I'm a big believer programmers should write code and if you're a programmer AND gonna write a blog, you should write the blog engine yourself. It shows off your talents, or lack there of, and you'll learn something along the way.
should_publish: yes
published: 2014-01-01T16:43:08.111Z
---
# I'm a programmer, I'm coding my own blog engine

This has been a long time coming. I've preached about blogging for years and yet, have never done it myself. Well, I have but back in 1997 and I didn't keep it up and I don't have proof :(. [side story: A colleague had a 4 seater, Beech Debonair, and flew 3 of us from the Clear Lake area near Houston, up through Oklahoma, into Jackson Hole Wyoming, over to Seattle around Mount Rainier, then Vancouver, up the inside passage to Ketchikan and Juneau Alaska, and back. I blogged about the trip. Family and friends back home kept up with our adventures. I lost it over the years.] And I'm a big believer programmers should write code and if you're a programmer and gonna write a blog, it's a great opportunity to write the blog engine yourself. It's a good learning opportunity and realtively simple.

The kick in the butt was when Kijana started his own blog and [coded it himself](http://kijanawoodard.com/building-a-blog-engine) and Zach picked up Moonscript and [built a blog engine](http://www.throw-up.com/building-openresty); so it's about damn time I did it.

I like Javascript. [So I've built a static site generator in Javascript using Node JS](https://gitlab.com/joey/joey.gitlab.io). So let's begin.