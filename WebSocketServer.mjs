import SocketIo from "socket.io"
import Url from "url"
const defaultDelegate = {
    left(room, socket){},
    joined(room, socket){},
    update(message, room, socket){},
    messageFromUser(message){}
}

const WebSocketServer = (server, delegate = {}) => {
    delegate = Object.assign(defaultDelegate, delegate)
    const io = SocketIo(server)
    io.on("connection", socket => {
        let room = WebSocketServer.parseForRoom(socket.handshake.headers.referer)
        room = room.indexOf(".html") > -1 ? room : `${room}index.html`
        socket.join(room)
        delegate.joined(room, socket)
        //socket.emit("joined", {user: socket.request.user, room: room})
        io.to(room).emit("joined", {user: socket.request.user, room: room})
        //console.log(socket.adapter.rooms)
        //console.log("connected\n", Object.keys(io.clients().connected).map(id=>io.clients().connected[id].rooms))    
        socket.on("disconnect", message=>{
            console.log("logged off", message)
            socket.leave(room)
            delegate.left(room, socket)
        })
        socket.on("update", message => {
            console.log("update", message)
            delegate.updated(message, room, socket)
        })
        socket.on("sending message from user", message => {
            delegate.messageFromUser(message)
        })
    })
    return io   
}
WebSocketServer.parseForRoom = url => {
    return Url.parse(url).pathname
}
export default WebSocketServer